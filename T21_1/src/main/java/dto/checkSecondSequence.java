package dto;

public class checkSecondSequence {

	static String secondSequence = "";
	
	public checkSecondSequence(String secondSequence) {
		
		this.secondSequence = secondSequence;
		
	}

	public static String getSecondSequence() {
		
		return secondSequence;
		
	}

	public static void setSecondSequence(String secondSequence) {
		
		checkSecondSequence.secondSequence = secondSequence;
		
	}
	
}