package dto;

public class checkOperator {

	static String operator = "";
	
	public checkOperator(String operator) {
		
		this.operator = operator;
		
	}

	public static String getOperator() {
		
		return operator;
		
	}

	public static void setOperator(String operator) {
		
		checkOperator.operator = operator;
		
	}
	
}