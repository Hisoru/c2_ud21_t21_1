package C2_UD21.T21_1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.*;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import java.awt.Component;

public class mainApp extends JFrame {

	private JPanel contentPane;
	private JTextField numberTextField;
	private JTextField pastNumberTextField;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					mainApp frame = new mainApp();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
				
			}
			
		});
		
	}

	public mainApp() {
		setTitle("Calculator");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 502, 495);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar mainMenuBar = new JMenuBar();
		mainMenuBar.setBounds(0, 0, 17, 21);
		contentPane.add(mainMenuBar);
		
		JMenu mainMenu = new JMenu("=");
		mainMenuBar.add(mainMenu);
		
		JMenuItem standardMenuItem = new JMenuItem("Standard");
		standardMenuItem.setSelected(true);
		mainMenu.add(standardMenuItem);
		
		JMenuItem scientificMenuItem = new JMenuItem("Scientific");
		mainMenu.add(scientificMenuItem);
		
		JLabel standardLabel = new JLabel("Standard");
		standardLabel.setBounds(27, 3, 60, 14);
		contentPane.add(standardLabel);
		
		JButton windowButton = new JButton(">");
		windowButton.setBounds(97, -2, 41, 23);
		contentPane.add(windowButton);
		
		JMenuItem historyMenuItem = new JMenuItem("History");
		historyMenuItem.setBounds(326, -1, 71, 22);
		contentPane.add(historyMenuItem);
		
		JMenuItem memoryMenuItem = new JMenuItem("Memory");
		memoryMenuItem.setBounds(407, 0, 80, 22);
		contentPane.add(memoryMenuItem);
		
		final TextArea historyTextArea = new TextArea();
		historyTextArea.setEditable(false);
		historyTextArea.setBounds(326, 28, 161, 429);
		contentPane.add(historyTextArea);
		
		numberTextField = new JTextField();
		numberTextField.setEditable(false);
		numberTextField.setBounds(0, 72, 320, 40);
		contentPane.add(numberTextField);
		numberTextField.setColumns(10);
		
		JButton mcButton = new JButton("MC");
		mcButton.setBounds(0, 123, 60, 23);
		contentPane.add(mcButton);
		
		JButton mrButton = new JButton("MR");
		mrButton.setBounds(60, 123, 60, 23);
		contentPane.add(mrButton);
		
		JButton mPlusButton = new JButton("M+");
		mPlusButton.setBounds(120, 123, 60, 23);
		contentPane.add(mPlusButton);
		
		JButton mMinusButton = new JButton("M-");
		mMinusButton.setBounds(180, 123, 60, 23);
		contentPane.add(mMinusButton);
		
		JButton msButton = new JButton("MS");
		msButton.setBounds(240, 123, 60, 23);
		contentPane.add(msButton);
		
		JButton percentageButton = new JButton("%");
		percentageButton.setBounds(0, 157, 80, 50);
		contentPane.add(percentageButton);
		
		JButton ceButton = new JButton("CE");
		ceButton.setBounds(80, 157, 80, 50);
		contentPane.add(ceButton);
		
		JButton cButton = new JButton("C");
		cButton.setBounds(160, 157, 80, 50);
		contentPane.add(cButton);
		
		JButton deleteButton = new JButton("<x|");
		deleteButton.setBounds(240, 157, 80, 50);
		contentPane.add(deleteButton);
		
		JButton divisionOneButton = new JButton("1/x");
		divisionOneButton.setBounds(0, 207, 80, 50);
		contentPane.add(divisionOneButton);
		
		JButton sevenButton = new JButton("7");
		sevenButton.setBounds(0, 257, 80, 50);
		contentPane.add(sevenButton);
		
		JButton fourButton = new JButton("4");
		fourButton.setToolTipText("");
		fourButton.setBounds(0, 307, 80, 50);
		contentPane.add(fourButton);
		
		JButton oneButton = new JButton("1");
		oneButton.setBounds(0, 357, 80, 50);
		contentPane.add(oneButton);
		
		JButton positiveNegativeButton = new JButton("+/_");
		positiveNegativeButton.setBounds(0, 407, 80, 50);
		contentPane.add(positiveNegativeButton);
		
		JButton powerButton = new JButton("x2");
		powerButton.setBounds(80, 207, 80, 50);
		contentPane.add(powerButton);
		
		JButton eightButton = new JButton("8");
		eightButton.setBounds(80, 257, 80, 50);
		contentPane.add(eightButton);
		
		JButton fiveButton = new JButton("5");
		fiveButton.setBounds(80, 307, 80, 50);
		contentPane.add(fiveButton);
		
		JButton twoButton = new JButton("2");
		twoButton.setBounds(80, 357, 80, 50);
		contentPane.add(twoButton);
		
		JButton zeroButton = new JButton("0");
		zeroButton.setBounds(80, 407, 80, 50);
		contentPane.add(zeroButton);
		
		JButton squareButton = new JButton("2Vx");
		squareButton.setBounds(160, 207, 80, 50);
		contentPane.add(squareButton);
		
		JButton nineButton = new JButton("9");
		nineButton.setBounds(160, 257, 80, 50);
		contentPane.add(nineButton);
		
		JButton sixButton = new JButton("6");
		sixButton.setBounds(160, 307, 80, 50);
		contentPane.add(sixButton);
		
		JButton threeButton = new JButton("3");
		threeButton.setBounds(160, 357, 80, 50);
		contentPane.add(threeButton);
		
		JButton dotButton = new JButton(".");
		dotButton.setBounds(160, 407, 80, 50);
		contentPane.add(dotButton);
		
		JButton divideButton = new JButton("/");
		divideButton.setBounds(240, 207, 80, 50);
		contentPane.add(divideButton);
		
		JButton multiplicationButton = new JButton("*");
		multiplicationButton.setBounds(240, 257, 80, 50);
		contentPane.add(multiplicationButton);
		
		JButton substractButton = new JButton("-");
		substractButton.setBounds(240, 307, 80, 50);
		contentPane.add(substractButton);
		
		JButton additionButton = new JButton("+");
		additionButton.setBounds(240, 357, 80, 50);
		contentPane.add(additionButton);
		
		JButton equalsButton = new JButton("=");
		equalsButton.setToolTipText("");
		equalsButton.setBounds(240, 407, 80, 50);
		contentPane.add(equalsButton);
		
		zeroButton.setBackground(Color.WHITE);
		oneButton.setBackground(Color.WHITE);
		twoButton.setBackground(Color.WHITE);
		threeButton.setBackground(Color.WHITE);
		fourButton.setBackground(Color.WHITE);
		fiveButton.setBackground(Color.WHITE);
		sixButton.setBackground(Color.WHITE);
		sevenButton.setBackground(Color.WHITE);
		eightButton.setBackground(Color.WHITE);
		nineButton.setBackground(Color.WHITE);
		positiveNegativeButton.setBackground(Color.WHITE);
		dotButton.setBackground(Color.WHITE);
		equalsButton.setBackground(Color.LIGHT_GRAY);
		
		pastNumberTextField = new JTextField();
		pastNumberTextField.setEditable(false);
		pastNumberTextField.setColumns(10);
		pastNumberTextField.setBounds(0, 28, 320, 40);
		contentPane.add(pastNumberTextField);
		
		ceButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText("");
				pastNumberTextField.setText("");
				
			}
			
		});
		
		cButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText("");
				pastNumberTextField.setText("");
				
			}
			
		});
		
		deleteButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText("");
				pastNumberTextField.setText("");
				
			}
			
		});
		
		positiveNegativeButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				positiveNegativeCounter.setCounter(positiveNegativeCounter.getCounter() + 1);
				
				if (positiveNegativeCounter.getCounter() == 1) {
					
					numberTextField.setText("-" + numberTextField.getText());
					positiveNegativeCounter.setCounter(positiveNegativeCounter.getCounter() + 1);
					
				} else if (positiveNegativeCounter.getCounter() == 2) {
					
					numberTextField.setText(numberTextField.getText());
					positiveNegativeCounter.setCounter(0);
					
				}
				
			}
			
		});
		
		zeroButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 0);
				
			}
			
		});
		
		oneButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 1);
				
			}
			
		});
		
		twoButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 2);
				
			}
			
		});
		
		threeButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 3);
				
			}
			
		});
		
		fourButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 4);
				
			}
			
		});
		
		fiveButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 5);
				
			}
			
		});
		
		sixButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 6);
				
			}
			
		});
		
		sevenButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 7);
				
			}
			
		});
		
		eightButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 8);
				
			}
			
		});
		
		nineButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				numberTextField.setText(numberTextField.getText() + 9);
				
			}
			
		});
		
		additionButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				checkFirstSequence.setFirstSequence(numberTextField.getText());
				checkOperator.setOperator("+");
				
				numberTextField.setText("");
				pastNumberTextField.setText(checkFirstSequence.getFirstSequence() + " +");
				
			}
			
		});
		
		substractButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				checkFirstSequence.setFirstSequence(numberTextField.getText());
				checkOperator.setOperator("-");
				
				numberTextField.setText("");
				pastNumberTextField.setText(checkFirstSequence.getFirstSequence() + " -");
				
			}
			
		});
		
		multiplicationButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				checkFirstSequence.setFirstSequence(numberTextField.getText());
				checkOperator.setOperator("*");
				
				numberTextField.setText("");
				pastNumberTextField.setText(checkFirstSequence.getFirstSequence() + " *");
				
			}
			
		});
		
		divideButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				checkFirstSequence.setFirstSequence(numberTextField.getText());
				checkOperator.setOperator("/");
				
				numberTextField.setText("");
				pastNumberTextField.setText(checkFirstSequence.getFirstSequence() + " /");
				
			}
			
		});
		
		equalsButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				checkSecondSequence.setSecondSequence((numberTextField.getText()));
				
				int firstSequence = Integer.parseInt(checkFirstSequence.getFirstSequence());
				int secondSequence = Integer.parseInt(checkSecondSequence.getSecondSequence());
				
				if (checkOperator.getOperator().equalsIgnoreCase("+")) {
					
					Double doubleFirstSequence = new Double(firstSequence);
					Double doubleSecondSequence = new Double(firstSequence);
					
					boolean booleanFirstSequence = doubleFirstSequence.isNaN();
					boolean booleanSecondSequence = doubleSecondSequence.isNaN();
					
					if (!booleanFirstSequence && !booleanSecondSequence) {
						
						try {
							
							int result = firstSequence + secondSequence;
							
							String resultString = String.valueOf(result);
							
							numberTextField.setText(resultString);
							pastNumberTextField.setText(checkFirstSequence.getFirstSequence() + " " + checkOperator.getOperator() + " " + checkSecondSequence.getSecondSequence());

							historyTextArea.setText(historyTextArea.getText() + checkFirstSequence.getFirstSequence() + " " + checkOperator.getOperator() + " " + checkSecondSequence.getSecondSequence() + " = " + resultString + "\n");
							
						} catch (Exception e2) {
							
							JOptionPane.showMessageDialog(null, "Introduce un número");
							
						}
						
					}
					
				} else if (checkOperator.getOperator().equalsIgnoreCase("-")) {
					
					int result = firstSequence - secondSequence;
					
					String resultString = String.valueOf(result);
					
					numberTextField.setText(resultString);
					pastNumberTextField.setText(checkFirstSequence.getFirstSequence() + " " + checkOperator.getOperator() + " " + checkSecondSequence.getSecondSequence());

					historyTextArea.setText(historyTextArea.getText() + checkFirstSequence.getFirstSequence() + " " + checkOperator.getOperator() + " " + checkSecondSequence.getSecondSequence() + " = " + resultString + "\n");
					
				} else if (checkOperator.getOperator().equalsIgnoreCase("*")) {
					
					int result = firstSequence * secondSequence;
					
					String resultString = String.valueOf(result);
					
					numberTextField.setText(resultString);
					pastNumberTextField.setText(checkFirstSequence.getFirstSequence() + " " + checkOperator.getOperator() + " " + checkSecondSequence.getSecondSequence());
					
					historyTextArea.setText(historyTextArea.getText() + checkFirstSequence.getFirstSequence() + " " + checkOperator.getOperator() + " " + checkSecondSequence.getSecondSequence() + " = " + resultString + "\n");
					
				} else if (checkOperator.getOperator().equalsIgnoreCase("/")) {
					
					double result = firstSequence / secondSequence;
					
					String resultString = String.valueOf(result);
					
					numberTextField.setText(resultString);
					pastNumberTextField.setText(checkFirstSequence.getFirstSequence() + " " + checkOperator.getOperator() + " " + checkSecondSequence.getSecondSequence());
					
					historyTextArea.setText(historyTextArea.getText() + checkFirstSequence.getFirstSequence() + " " + checkOperator.getOperator() + " " + checkSecondSequence.getSecondSequence() + " = " + resultString + "\n");
					
				}
					
			}
			
		});
		
	}
	
}
