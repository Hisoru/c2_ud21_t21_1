package C2_UD21.T21_1;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import C2_UD21.T21_1.mainApp;
import dto.*;

public class AppTest extends TestCase {
	
	public void testGetFirstSequence() {
		
		assertEquals("1", checkFirstSequence.getFirstSequence());
		
	}
	
	public void testSetFirstSequence() {
		
		checkFirstSequence.setFirstSequence("1");
		assertEquals("1", checkFirstSequence.getFirstSequence());
		
	}
	
	public void testGetOperator() {
		
		assertEquals("+", checkOperator.getOperator());
		
	}
	
	public void testSetOperator() {
		
		checkOperator.setOperator("+");
		assertEquals("+", checkOperator.getOperator());
		
	}
	
	public void testGetSecondSequence() {
		
		assertEquals("1", checkSecondSequence.getSecondSequence());
		
	}
	
	public void testSetSecondSequence() {
		
		checkSecondSequence.setSecondSequence("1");
		assertEquals("1", checkSecondSequence.getSecondSequence());
		
	}
	
	public void testGetPositiveNegative() {
		
		assertEquals(1, positiveNegativeCounter.getCounter());
		
	}
	
	public void testSetPositiveNegative() {
		
		positiveNegativeCounter.setCounter(1);
		assertEquals(1, positiveNegativeCounter.getCounter());
		
	}
	
}